# AirQuality

## ¿De qué trata el proyecto?

Este proyecto forma parte de mi trabajo presentado para el módulo de Proyecto Integrado del grado superior de Desarrollo de Aplicaciones Multiplataforma.

La temática propuesta giraba entorno a la contaminación, un grave problema que nos afecta a todos. De este modo, la aplicación cueta con tres apartados claves:
- Calidad del aire de un sensor propio en el hogar o lugar de trabajo
- Calidad del aire de diferentes regiones y ciudades de España
- Última noticias sobre la contamiación

## ¿Cómo se puede poner en marcha?

Una vez descargado el repositorio debemos abrilo con Android Studio. No podremos probarlo tan cual, pues antes será necesario optener una API Key de los siguientes sitios:
- **Sensor Local**

https://firebase.google.com/products/realtime-database?hl=es-419

Se debe incluir la API Key en: /app/java/com/angel/airquality/viewModel/LocalSensorsViewModel

- **Sensores Externos**

https://aqicn.org/api/es/

Se debe incluir la API Key en: /app/java/com/angel/airquality/viewModel/ExternalSensorsViewModel

- **Últimas Noticias**

https://mediastack.com/

Se debe incluir la API Key en: /app/java/com/angel/airquality/viewModel/SplashViewModel

Tras incluir las tres API Keys, la aplicación ya se podrá ejecutar con normalidad. Es importante tener en cuenta las límitaciones de los planes gratuitos de Firebase y Mediastack si vamos a hacer un uso intensivo de la aplicación.
